package tb.sockets.client;

import javax.swing.*;
import java.awt.*;

public class Kwadrat extends JPanel {
    JLabel label = new JLabel((Icon)null);

    public Kwadrat() {
        setBackground(Color.white);
        add(label);
    }

    public void ustawIkone(Icon icon) {
        label.setIcon(icon);
    }
}