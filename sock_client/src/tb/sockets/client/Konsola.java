package tb.sockets.client;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Konsola {

    private JFrame frame = new JFrame("Kółko i krzyżyk");
    private JLabel messageLabel = new JLabel("");
    private ImageIcon ikona;
    private ImageIcon ikonaPrzeciwnika;

    private Kwadrat[] plansza = new Kwadrat[9];
    private Kwadrat aktualnyKwadrat;

    private BufferedImage redX;
    private BufferedImage redCircle;

    private static int PORT = 8900;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    public Konsola(String serverAddress) throws Exception {

        //uruchomienie socketa
        socket = new Socket(serverAddress, PORT);
        in = new BufferedReader(new InputStreamReader(
                socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);

        //rozklad GUI
        messageLabel.setBackground(Color.lightGray);
        frame.getContentPane().add(messageLabel, "South");

        JPanel planszaPanel = new JPanel();
        planszaPanel.setBackground(Color.black);
        planszaPanel.setLayout(new GridLayout(3, 3, 2, 2));
        zaladujZnaki();
        for (int i = 0; i < plansza.length; i++) {
            final int j = i;
            plansza[i] = new Kwadrat();
            plansza[i].addMouseListener(new MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    aktualnyKwadrat = plansza[j];
                    out.println("MOVE " + j);}});
            planszaPanel.add(plansza[i]);
        }
        frame.getContentPane().add(planszaPanel, "Center");
    }

    public void play() throws Exception {
        String sygnal;
        try {
            sygnal = in.readLine();
            if (sygnal.startsWith("WELCOME")) {
                char mark = sygnal.charAt(8);
                ikona = new ImageIcon(mark == 'X' ? redX : redCircle);
                ikonaPrzeciwnika  = new ImageIcon(mark == 'X' ? redCircle : redX);
                frame.setTitle("Kolko i krzyzyk - " + mark);
            }
            while (true) {
                sygnal = in.readLine();
                if (sygnal.startsWith("VALID_MOVE")) {
                    messageLabel.setText("Poprawny ruch, ruch przeciwnika");
                    aktualnyKwadrat.ustawIkone(ikona);
                    aktualnyKwadrat.repaint();
                } else if (sygnal.startsWith("OPPONENT_MOVED")) {
                    int loc = Integer.parseInt(sygnal.substring(14));
                    plansza[loc].ustawIkone(ikonaPrzeciwnika);
                    plansza[loc].repaint();
                    messageLabel.setText("Przeciwnik wykonal ruch, twoja kolej.");
                } else if (sygnal.startsWith("VICTORY")) {
                    messageLabel.setText("Wygrywasz");
                    break;
                } else if (sygnal.startsWith("DEFEAT")) {
                    messageLabel.setText("Przegrywasz");
                    break;
                } else if (sygnal.startsWith("TIE")) {
                    messageLabel.setText("Remis");
                    break;
                } else if (sygnal.startsWith("MESSAGE")) {
                    messageLabel.setText(sygnal.substring(8));
                }
            }
            out.println("QUIT");
        }
        finally {
            socket.close();
        }
    }

    private boolean jeszczeRaz() {
        int sygnal = JOptionPane.showConfirmDialog(frame,
                "Jeszcze raz?",
                "Jeszcze raz?",
                JOptionPane.YES_NO_OPTION);
        frame.dispose();
        return sygnal == JOptionPane.YES_OPTION;
    }


    private void zaladujZnaki() {
        try {
            redX = ImageIO.read(getClass().getClassLoader().getResource("resources/redX.png"));
            redCircle = ImageIO.read(getClass().getClassLoader().getResource("resources/redCircle.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Runs the client as an application.
     */
    public static void main(String[] args) throws Exception {
        while (true) {
            String serverAddress = (args.length == 0) ? "localhost" : args[1];
            Konsola client = new Konsola(serverAddress);
            client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            client.frame.setSize(260, 260);
            client.frame.setVisible(true);
            client.frame.setResizable(false);
            client.play();
            if (!client.jeszczeRaz()) {
                break;
            }
        }
    }
}