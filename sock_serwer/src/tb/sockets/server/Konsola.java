package tb.sockets.server;

import java.net.ServerSocket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Konsola {

    public static void main(String[] args) throws Exception {
        ServerSocket listener = new ServerSocket(8900);
        System.out.println("Serwer dziala...");
        try {
            while (true) {
                Gra gra = new Gra();
                Gra.Gracz graczX = gra.new Gracz(listener.accept(), 'X');
                Gra.Gracz graczO = gra.new Gracz(listener.accept(), 'O');
                graczX.setOpponent(graczO);
                graczO.setOpponent(graczX);
                gra.aktualnyGracz = graczX;
                graczX.start();
                graczO.start();
            }
        } finally {
            listener.close();
        }
    }
}

class Gra {

    private Gracz[] plansza = {
            null, null, null,
            null, null, null,
            null, null, null};

    Gracz aktualnyGracz;


    public boolean czyJestZwyciezca() {
        return
                (plansza[0] != null && plansza[0] == plansza[1] && plansza[0] == plansza[2])
                        ||(plansza[3] != null && plansza[3] == plansza[4] && plansza[3] == plansza[5])
                        ||(plansza[6] != null && plansza[6] == plansza[7] && plansza[6] == plansza[8])
                        ||(plansza[0] != null && plansza[0] == plansza[3] && plansza[0] == plansza[6])
                        ||(plansza[1] != null && plansza[1] == plansza[4] && plansza[1] == plansza[7])
                        ||(plansza[2] != null && plansza[2] == plansza[5] && plansza[2] == plansza[8])
                        ||(plansza[0] != null && plansza[0] == plansza[4] && plansza[0] == plansza[8])
                        ||(plansza[2] != null && plansza[2] == plansza[4] && plansza[2] == plansza[6]);
    }


    public boolean czyPlanszaZapelniona() {
        for (int i = 0; i < plansza.length; i++) {
            if (plansza[i] == null) {
                return false;
            }
        }
        return true;
    }

    public synchronized boolean poprawnyRuch(int pole, Gracz gracz) {
        if (gracz == aktualnyGracz && plansza[pole] == null) {
            plansza[pole] = aktualnyGracz;
            aktualnyGracz = aktualnyGracz.opponent;
            aktualnyGracz.ruchPrzeciwnika(pole);
            return true;
        }
        return false;
    }

    class Gracz extends Thread {
        char znak;
        Gracz opponent;
        Socket socket;
        BufferedReader input;
        PrintWriter output;


        public Gracz(Socket socket, char znak) {
            this.socket = socket;
            this.znak = znak;
            try {
                input = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                output = new PrintWriter(socket.getOutputStream(), true);
                output.println("WELCOME " + znak);
                output.println("MESSAGE Czekam na drugiego gracza");
            } catch (IOException e) {
                System.out.println("Gracz odszedl: " + e);
            }
        }


        public void setOpponent(Gracz opponent) {
            this.opponent = opponent;
        }


        public void ruchPrzeciwnika(int pole) {
            output.println("OPPONENT_MOVED" + pole);
            output.println(
                    czyJestZwyciezca() ? "DEFEAT" : czyPlanszaZapelniona() ? "TIE" : "");
        }


        public void run() {
            try {
                // gdy obaj sie polacza.
                output.println("MESSAGE Obaj gracze sie polaczyli");

                // informowanie o ruchu
                if (znak == 'X') {
                    output.println("MESSAGE Twoj ruch");
                }

                // Otrzymywanie rozkazow i egzekucja.
                while (true) {
                    String command = input.readLine();
                    if (command.startsWith("MOVE")) {
                        int pole = Integer.parseInt(command.substring(5));
                        if (poprawnyRuch(pole, this)) {
                            output.println("VALID_MOVE");
                            output.println(czyJestZwyciezca() ? "VICTORY"
                                    : czyPlanszaZapelniona() ? "TIE"
                                    : "");
                        } else {
                            output.println("MESSAGE ?");
                        }
                    } else if (command.startsWith("QUIT")) {
                        return;
                    }
                }
            } catch (IOException e) {
                System.out.println("Gracz odszedl: " + e);
            } finally {
                try {socket.close();} catch (IOException e) {}
            }
        }
    }
}